package edu.usu.cs.useal.rabbitmq;

import java.util.Dictionary;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;

import edu.usu.cs.useal.core.configuration.UsealManagedService;

public class RabbitMqConfigurationService extends UsealManagedService {
	private static final Logger logger = Logger.getLogger(RabbitMqConfigurationService.class);

	private static final String HOST = "edu.usu.cs.useal.rabbitmq.host";

	private static final String CHANNEL_REQUEST = "edu.usu.cs.useal.rabbitmq.channel.request";

	private static final String CHANNEL_RESULT = "edu.usu.cs.useal.rabbitmq.channel.result";

	private final BundleContext mContext;

	private ServiceRegistration<RabbitMqService> mRabbitMqServiceReg;

	ServiceFactory<RabbitMqService> mRabbitMqServiceFactory;

	public RabbitMqConfigurationService(BundleContext bundleContext) {
		mContext = bundleContext;
	}

	@Override
	public void configOnce(Dictionary config) throws ConfigurationException {
		String host = (String) config.get(HOST);
		String requestChannel = (String) config.get(CHANNEL_REQUEST);
		String resultChannel = (String) config.get(CHANNEL_RESULT);

		logger.info("RabbitMQ client initilizing...!");

		mRabbitMqServiceFactory = new RabbitMqServiceFactory(host, requestChannel, resultChannel);
		mRabbitMqServiceReg = mContext.registerService(RabbitMqService.class, mRabbitMqServiceFactory, new Hashtable());

		logger.info("RabbitMQ client registered...!");
	}

	@Override
	public void configUpdateable(Dictionary config) throws ConfigurationException {
		// No service configUpdateable
	}

	@Override
	public void stopService() {
		mRabbitMqServiceReg.unregister();
	}

}
