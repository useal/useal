package edu.usu.cs.useal.rabbitmq;

import com.rabbitmq.client.Channel;

import edu.usu.cs.useal.task.queue.TaskQueueService;
import edu.usu.cs.useal.task.request.Request;
import edu.usu.cs.useal.task.request.RequestListener;
import edu.usu.cs.useal.task.result.Result;
import edu.usu.cs.useal.task.result.ResultListener;

public class RabbitMqService implements TaskQueueService {
	private final Channel mRabbitMqChannel;
	private final String mRequestChannel;
	private final String mResultChannel;

	public RabbitMqService(Channel rabbitMqChannel, String requestChannel, String resultChannel) {
		mRabbitMqChannel = rabbitMqChannel;
		mRequestChannel = requestChannel;
		mResultChannel = resultChannel;
	}

	@Override
	public void queueRequest(Request request) {
		// TODO Auto-generated method stub

	}

	@Override
	public void listenToRequest(RequestListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void queueResult(Result result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void listenToResult(ResultListener listener) {
		// TODO Auto-generated method stub

	}
}
