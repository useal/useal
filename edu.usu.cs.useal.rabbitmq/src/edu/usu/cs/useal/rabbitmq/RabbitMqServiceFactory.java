/**
 * 
 */
package edu.usu.cs.useal.rabbitmq;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitMqServiceFactory implements ServiceFactory<RabbitMqService> {
	private static final Logger logger = Logger.getLogger(RabbitMqServiceFactory.class);

	private String mHost;
	private String mRequestChannel;
	private String mResultChannel;

	RabbitMqServiceFactory(String host, String requestChannel, String resultChannel) {
		mHost = host;
		mRequestChannel = requestChannel;
		mResultChannel = resultChannel;
	}

	@Override
	public RabbitMqService getService(Bundle bundle, ServiceRegistration<RabbitMqService> registration) {
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(mHost);
			Connection connection = factory.newConnection();
			Channel rabbitMqChannel;
			rabbitMqChannel = connection.createChannel();

			return new RabbitMqService(rabbitMqChannel, mRequestChannel, mResultChannel);
		} catch (IOException | TimeoutException e) {
			logger.error("Error while create channel to RabbitMQ server", e);
			return null;
		}
	}

	@Override
	public void ungetService(Bundle bundle, ServiceRegistration<RabbitMqService> registration,
			RabbitMqService service) {
	}

}
