package edu.usu.cs.useal.core.configuration;

import java.util.Dictionary;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

public abstract class UsealManagedService implements ManagedService {

	private boolean isUpdatedOnce;

	public UsealManagedService() {
		isUpdatedOnce = false;
	}

	@Override
	public void updated(Dictionary config) throws ConfigurationException {
		if (config == null) {
			return;
		} else {
			// If not yet update once then run config once
			if (!isUpdatedOnce) {
				configOnce(config);
				isUpdatedOnce = true;
			}
			// Always run configUpdateable
			configUpdateable(config);
		}
	}

	abstract public void configOnce(Dictionary config) throws ConfigurationException;

	abstract public void configUpdateable(Dictionary config) throws ConfigurationException;

	abstract public void stopService();
}
