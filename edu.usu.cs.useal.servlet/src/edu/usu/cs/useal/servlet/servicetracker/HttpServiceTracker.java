package edu.usu.cs.useal.servlet.servicetracker;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;
import org.eclipse.equinox.http.servlet.ExtendedHttpService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.NamespaceException;
import org.osgi.util.tracker.ServiceTracker;

import edu.usu.cs.useal.servlet.git.GitTestServlet;

public class HttpServiceTracker extends ServiceTracker {

	private static final Logger logger = Logger.getLogger(HttpServiceTracker.class);

	public List<String> mRegistedAliases;

	// private Filter mShirofilter;

	public HttpServiceTracker(BundleContext context) {
		super(context, ExtendedHttpService.class.getName(), null);
		mRegistedAliases = new LinkedList<>();
	}

	@Override
	public Object addingService(ServiceReference reference) {
		// HttpService httpService2 = (HttpService)
		// super.addingService(reference);
		ExtendedHttpService xHttpService = (ExtendedHttpService) super.addingService(reference);
		if (xHttpService == null)
			return null;
		try {
			logger.info("Registering servlet...");

			addServlet(xHttpService, GitTestServlet.SERVLET_ALIAS, new GitTestServlet());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return xHttpService;
	}

	private void addServlet(ExtendedHttpService xHttpService, String alias, HttpServlet servlet)
			throws ServletException, NamespaceException {
		xHttpService.registerServlet(alias, servlet, null, null);
		mRegistedAliases.add(alias);
	}

	@Override
	public void removedService(ServiceReference reference, Object service) {
		ExtendedHttpService xHttpService = (ExtendedHttpService) service;

		// TODO:Put in log system
		System.out.println("Unregistering all");

		// Unregister servlet
		for (String alias : mRegistedAliases) {
			xHttpService.unregister(alias);
		}

		// Unregister filter
		// xHttpService.unregisterFilter(mShirofilter);

		// Remove service
		super.removedService(reference, service);
	}
}
