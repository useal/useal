package edu.usu.cs.useal.servlet.git;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import edu.usu.cs.useal.servlet.Activator;
import edu.usu.cs.useal.task.queue.TaskQueueService;
import edu.usu.cs.useal.task.request.Request;
import edu.usu.cs.useal.task.result.Result;
import edu.usu.cs.useal.task.result.ResultListener;

public class GitTestServlet extends HttpServlet {

	private static final Logger logger = Logger.getLogger(GitTestServlet.class);

	public static final String SERVLET_ALIAS = "/gitTest";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		TaskQueueService taskQueueService = Activator.getTaskQueueService();
		taskQueueService.listenToResult(new ResultListener() {
			@Override
			public void onResult(Result result) {
				logger.debug("Request: " + result.getId() + " is done!!!");
			}
		});

		taskQueueService.queueRequest(new Request() {
			@Override
			public String getType() {
				return "gitClone";
			}

			@Override
			public String getRequest() {
				return "git clone https://hvpham@bitbucket.org/useal/defectdetectionsemantic.git";
			}
		});
	}
}
