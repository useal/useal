package edu.usu.cs.useal.servlet;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import edu.usu.cs.useal.servlet.servicetracker.HttpServiceTracker;
import edu.usu.cs.useal.task.queue.TaskQueueService;
import edu.usu.cs.useal.task.queue.TaskQueueServiceTracker;

public class Activator implements BundleActivator {

	private HttpServiceTracker mServiceTracker;

	private static TaskQueueServiceTracker mTaskQueueServiceTracker;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.
	 * BundleContext)
	 */
	@Override
	public void start(BundleContext bundleContext) throws Exception {
		// Start Http Service Tracker
		mServiceTracker = new HttpServiceTracker(bundleContext);
		mServiceTracker.open();

		// Start TaskQueue Tracker
		mTaskQueueServiceTracker = new TaskQueueServiceTracker(bundleContext);
		mTaskQueueServiceTracker.open();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		// Stop Http Service Tracker
		mServiceTracker.close();
		mServiceTracker = null;

		// Stop Data Persistence Tracker
		mTaskQueueServiceTracker.close();
		mTaskQueueServiceTracker = null;
	}

	public static TaskQueueService getTaskQueueService() {
		return mTaskQueueServiceTracker.getTaskQueueService();
	}
}
