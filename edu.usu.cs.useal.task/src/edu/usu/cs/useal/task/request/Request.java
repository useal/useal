package edu.usu.cs.useal.task.request;

public abstract class Request {

	private static long INIT_ID = 0;

	private final long mId;

	public Request() {
		mId = INIT_ID;
		INIT_ID++;
	}

	public long getId() {
		return mId;
	}

	abstract public String getType();

	abstract public String getRequest();
}
