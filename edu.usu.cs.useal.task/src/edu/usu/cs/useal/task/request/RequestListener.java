package edu.usu.cs.useal.task.request;

public interface RequestListener {
	public void onRequest(Request request);
}
