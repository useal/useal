package edu.usu.cs.useal.task.result;

import edu.usu.cs.useal.task.request.Request;

public abstract class Result {
	private final Request mRequest;

	public Result(Request request) {
		mRequest = request;
	}

	public long getId() {
		return mRequest.getId();
	}

	abstract public String getType();

	abstract public String getResult();
}
