package edu.usu.cs.useal.task.result;

public interface ResultListener {
	public void onResult(Result result);
}
