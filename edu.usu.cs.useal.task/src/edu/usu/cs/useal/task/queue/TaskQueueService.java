package edu.usu.cs.useal.task.queue;

import edu.usu.cs.useal.task.request.Request;
import edu.usu.cs.useal.task.request.RequestListener;
import edu.usu.cs.useal.task.result.Result;
import edu.usu.cs.useal.task.result.ResultListener;

public interface TaskQueueService {
	public void queueRequest(Request request);

	public void listenToRequest(RequestListener listener);

	public void queueResult(Result result);

	public void listenToResult(ResultListener listener);
}
