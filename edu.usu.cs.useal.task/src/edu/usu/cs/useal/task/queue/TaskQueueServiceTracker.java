package edu.usu.cs.useal.task.queue;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

public class TaskQueueServiceTracker extends ServiceTracker {

	private TaskQueueService mTaskQueueService;

	public TaskQueueServiceTracker(BundleContext context) {
		super(context, TaskQueueService.class.getName(), null);
	}

	@Override
	public Object addingService(ServiceReference reference) {
		mTaskQueueService = (TaskQueueService) super.addingService(reference);
		return mTaskQueueService;
	}

	@Override
	public void removedService(ServiceReference reference, Object service) {
		mTaskQueueService = null;
		super.removedService(reference, service);
	}

	public TaskQueueService getTaskQueueService() {
		return mTaskQueueService;
	}
}
